#! /bin/bash

if [[ $LANG == fr* ]]  ; then
    xdg-open https://degooglisons-internet.org/liste
elif [[ $LANG == es* ]] ; then
    xdg-open https://degooglisons-internet.org/list?l=es
else
    xdg-open https://degooglisons-internet.org/list?l=en
fi




