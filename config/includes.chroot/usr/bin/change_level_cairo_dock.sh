#! /bin/bash

# change_level_cairo_dock.sh --
#
#   This file handles the change level of the cairo-dock
#   in the Emmabuntüs distribution
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

clear

nom_distribution="Emmabuntus Debian Edition 5"

nom_logiciel_affichage="Enable/Disable Emmabuntus dock"

rm ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf

/usr/bin/start_cairo_dock.sh
