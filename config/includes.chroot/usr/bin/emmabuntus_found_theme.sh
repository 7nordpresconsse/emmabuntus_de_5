#!/bin/bash

# Emmabuntus_found_theme.sh --
#
#   This file permits to found color highlight of theme
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


function FOUND_HIGHLIGHT_COLOR()
{

theme=$(xfconf-query -v -c xsettings -p /Net/ThemeName)

if [[ ${theme} == Arc-Dark || ${theme} == Arc-Dark-largerborders || ${theme} == Blackbird || ${theme} == Xfce-dusk ]] ; then
    highlight_color=orange
elif [[ ${theme} == *-dark || ${theme} == *-dark-largerborders ]] ; then
    highlight_color=orange
else
    highlight_color=blue
fi

echo "${highlight_color}"

}


function FOUND_THEME_DARK()
{

theme=$(xfconf-query -v -c xsettings -p /Net/ThemeName)

if [[ ${theme} == Arc-Darker-largerborders || ${theme} == Arc-largerborders ]] ; then
    theme_dark="Arc-Dark-largerborders"
elif [[ ${theme} == Qogir-light-largerborders || ${theme} == Qogir-largerborders ]] ; then
    theme_dark="Qogir-dark-largerborders"
elif [[ ${theme} == Arc-Darker ]] ; then
    theme_dark="Arc-Dark"
elif [[ ${theme} == Qogir-light || ${theme} == Qogir ]] ; then
    theme_dark="Qogir-dark"
elif [[ ${theme} == Arc ]] ; then
    theme_dark="Blackbird"
elif [[ ${theme} == Adwaita ]] ; then
    theme_dark="Adwaita-dark"
elif [[ ${theme} == Xfce* ]] ; then
    theme_dark="Xfce-dusk"
else
    theme_dark=${theme}
fi

echo "${theme_dark}"

}

function FOUND_THEME_LIGHT()
{

theme=$(xfconf-query -v -c xsettings -p /Net/ThemeName)

if [[ ${theme} == Arc-Dark-largerborders ]] ; then
    theme_light="Arc-Darker-largerborders"
elif [[ ${theme} == Qogir-dark-largerborders ]] ; then
    theme_light="Qogir-largerborders"
elif [[ ${theme} == Arc-Dark ]] ; then
    theme_light="Arc-Darker"
elif [[ ${theme} == Qogir-dark ]] ; then
    theme_light="Qogir-light"
elif [[ ${theme} == Blackbird ]] ; then
    theme_light="Arc"
elif [[ ${theme} == Adwaita-dark ]] ; then
    theme_light="Adwaita"
elif [[ ${theme} == Xfce*-dusk ]] ; then
    theme_light="Xfce"
else
    theme_light=${theme}
fi

echo "${theme_light}"

}
